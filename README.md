Kuidas käivitada lahendust?
Kõige mugavam on seda teha kasutades Intellij IDE-ga. 
FILE -> NEW -> PROJECT FROM VERSION CONTROL. Seejärel küsitakse 
linki, milleks on https://bitbucket.org/kalev1984/cgisummeruni/
Projekti tööle saamiseks tuleb käivitada DentistAppApplication,
mille peale laetakse üles mälupõhine andmebaas ning server,
mis avaneb http://localhost:8080/

Esimene vaade on hambaarsti visiidile registreerumine. Kui minna
aadressile http://localhost:8080/dentist, avaneb võimalus hallata
arste. Vaikimisi on loodud kaks visiiti ning kaks arsti.

Lahendus algab entity kataloogist, kus meil on kaks olemit: arst ja visiit.
Seda kasutab programm andmete kirjutamiseks ja lugemiseks andmebaasist. Edasi on 
meil dto kaust. Seal on pealtnäha üks ühele sarnased failid olemitega, aga 
DTO-d kasutame kontrolleriga suhtlemiseks. Kaust mapper on mugavamaks üleminekuks
olemist DTO-ks ja vastupidi. See aitab koodi puhtamana ja selgemana hoida. Service
kataloogis on nii arsti kui visiidi jaoks oma nö. teenusepakkuja. Kui olemid ja DTO-d
koosnevad andmetest, siis teenusepakkuja koosneb tegevustest: CRUD(Create, Read,
Update, Delete) ja lisaks vajaminevad nagu näiteks otsing või topeltandmete kontroll.
Lõpuks on meil kontroller, kus on kirjeldatud kuhu iga leht suunatakse nii lingile
vajutamise korral, kui ka näiteks vigaste andmete sisestamise puhul.

resource kataloogis on veil vaatemudelid template kataloogis. Lisaks tõlked ning andmebaasi 
mudel, mille järgi luuakse tabelid ning seosed. 