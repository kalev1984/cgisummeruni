DROP TABLE IF EXISTS dentist;
DROP TABLE IF EXISTS dentist_visit;

CREATE TABLE dentist (
                        id INT AUTO_INCREMENT PRIMARY KEY,
                        firstname VARCHAR(255),
                        lastname VARCHAR(255),
                        phonenumber INT,
                        email VARCHAR(255)
);

CREATE TABLE dentist_visit (
                            id INT AUTO_INCREMENT PRIMARY KEY,
                            firstname VARCHAR(255),
                            lastname VARCHAR(255),
                            appointment TIMESTAMP,
                            phonenumber INT,
                            email VARCHAR(255),
                            dentist_id BIGINT,
                            FOREIGN KEY (dentist_id)
                                REFERENCES dentist ON DELETE CASCADE
);

INSERT INTO dentist (id, firstname, lastname, phonenumber, email)
    VALUES (1, 'Mati', 'Murakas', 123456, 'mati.murakas@dentist.com');
INSERT INTO dentist (id, firstname, lastname, phonenumber, email)
    VALUES (2, 'Kati', 'Murakas', 1235668, 'kati.murakas@dentist.com');
INSERT INTO dentist_visit (id, firstname, lastname, appointment, phonenumber, email, dentist_id)
VALUES (1, 'Mihkel', 'Mutt', '2021-05-03 11:11:00', 1111111, 'mutt@patsient.ee', 1);
INSERT INTO dentist_visit (id, firstname, lastname, appointment, phonenumber, email, dentist_id)
VALUES (2, 'Madis', 'Kukk', '2021-05-03 12:11:00', 2222222, 'kukk@patsient.ee', 2);