package com.cgi.dentistapp.service;

import com.cgi.dentistapp.dto.DentistDTO;
import com.cgi.dentistapp.dto.DentistVisitDTO;
import com.cgi.dentistapp.entity.DentistEntity;
import com.cgi.dentistapp.entity.DentistVisitEntity;
import com.cgi.dentistapp.mapper.Mapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Service
@Transactional
public class DentistVisitService {

    @Autowired
    private EntityManager entityManager;
    @Autowired
    private DentistService service;
    private Mapper mapper = new Mapper();

    @Transactional
    public void addVisit(DentistVisitDTO dentistVisitDTO) {
        DentistVisitEntity dentistVisitEntity = mapper.dentistVisitDtoToEntity(dentistVisitDTO);
        DentistDTO dentistDTO = service.getDentist(dentistVisitDTO.getDentistId());
        DentistEntity dentistEntity = mapper.dentistDtoToEntity(dentistDTO);
        dentistVisitEntity.setDentistEntity(dentistEntity);
        entityManager.persist(dentistVisitEntity);
    }

    public List<DentistVisitDTO> getVisits() {
        List<DentistVisitDTO> dentists = new ArrayList<>();
        List<DentistVisitEntity> entities =
                entityManager.createQuery("SELECT d FROM DentistVisitEntity d", DentistVisitEntity.class).getResultList();
        for (DentistVisitEntity entity : entities) {
            dentists.add(mapper.dentistVisitEntityToDto(entity));
        }
        return dentists;
    }

    public DentistVisitDTO getVisit(Long id) {
        DentistVisitEntity entity = entityManager.find(DentistVisitEntity.class, id);
        return mapper.dentistVisitEntityToDto(entity);
    }

    public boolean dateExists(Date inputDate, Long dentistId) {
        TypedQuery<DentistVisitEntity> query = entityManager.createQuery("SELECT d FROM DentistVisitEntity d WHERE d.date = ?1 " +
                "AND d.dentistEntity.id = ?2", DentistVisitEntity.class);
        List<DentistVisitEntity> visits = query.setParameter(1, inputDate).setParameter(2, dentistId).getResultList();
        return visits.size() == 0;
    }

    @Transactional
    public void updateVisit(DentistVisitDTO visit) {
        DentistVisitEntity entity = mapper.dentistVisitDtoToEntity(visit);
        if (entityManager.find(DentistVisitEntity.class, visit.getId()) == null) {
            entityManager.persist(entity);
        } else {
            entityManager.merge(entity);
        }
    }

    @Transactional
    public void deleteVisit(Long id) {
        DentistVisitEntity visit = entityManager.find(DentistVisitEntity.class, id);
        if (visit != null) {
            entityManager.remove(visit);
        }
    }

    public List<DentistVisitDTO> findVisits(String queryString) {
        TypedQuery<DentistVisitEntity> query = entityManager.createQuery(
                "SELECT d FROM DentistVisitEntity d WHERE d.firstName LIKE ?1 AND d.lastName LIKE ?1 " +
                        "AND d.email LIKE '?1'", DentistVisitEntity.class);
        return mapper.dentistVisitEntityListToVisitDtoList(query.setParameter(1, queryString).getResultList());
    }
}
