package com.cgi.dentistapp.service;

import com.cgi.dentistapp.dto.DentistDTO;
import com.cgi.dentistapp.entity.DentistEntity;
import com.cgi.dentistapp.mapper.Mapper;
import org.springframework.stereotype.Service;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;

@Service
@Transactional
public class DentistService {

    @PersistenceContext
    private EntityManager entityManager;

    private Mapper mapper = new Mapper();

    @Transactional
    public void addDentist(DentistDTO dentist) {
        entityManager.persist(mapper.dentistDtoToEntity(dentist));
    }

    public List<DentistDTO> getDentists() {
        List<DentistEntity> entities =
                entityManager.createQuery
                        ("SELECT d FROM DentistEntity d", DentistEntity.class).getResultList();
        List<DentistDTO> dentists = new ArrayList<>();
        for (DentistEntity e : entities) {
            dentists.add(mapper.dentistEntityToDto(e));
        }
        return dentists;
    }

    public DentistDTO getDentist(Long id) {
        return mapper.dentistEntityToDto
                (entityManager.find(DentistEntity.class, id));
    }

    @Transactional
    public void deleteDentist(Long id) {
        DentistEntity dentist = entityManager.find(DentistEntity.class, id);
        if (dentist != null) {
            entityManager.remove(dentist);
        }
    }

    @Transactional
    public void updateDentist(DentistDTO dentist) {
        DentistEntity entity = mapper.dentistDtoToEntity(dentist);
        if (entityManager.find(DentistEntity.class, dentist.getId()) == null) {
            entityManager.persist(entity);
        } else {
            entityManager.merge(entity);
        }
    }
}