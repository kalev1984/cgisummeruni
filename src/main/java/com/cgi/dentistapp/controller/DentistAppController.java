package com.cgi.dentistapp.controller;

import com.cgi.dentistapp.dto.DentistDTO;
import com.cgi.dentistapp.dto.DentistVisitDTO;
import com.cgi.dentistapp.service.DentistService;
import com.cgi.dentistapp.service.DentistVisitService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.config.annotation.ViewControllerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

import javax.validation.Valid;
import java.util.List;

@Controller
@EnableAutoConfiguration
public class DentistAppController extends WebMvcConfigurerAdapter {

    @Autowired
    private DentistVisitService dentistVisitService;
    @Autowired
    private DentistService dentistService;

    @Override
    public void addViewControllers(ViewControllerRegistry registry) {
        registry.addViewController("/visit").setViewName("visit/index");
        registry.addViewController("/dentist").setViewName("dentist/dentist");
    }

    @GetMapping("/")
    public String showRegisterForm(DentistVisitDTO dentistVisitDTO) {
        return "visit/index";
    }

    @GetMapping("/add")
    public String addVisit(DentistVisitDTO dentistVisitDTO, Model model) {
        List<DentistDTO> dentists = dentistService.getDentists();
        model.addAttribute("dentists", dentists);
        return "visit/addVisit";
    }

    @PostMapping("/add")
    public String registerVisit(@Valid DentistVisitDTO visitDTO, BindingResult bindingResult, Model model) {
        if (!dentistVisitService.dateExists(visitDTO.getDate(), visitDTO.getDentistId())) {
            ObjectError error = new ObjectError("date","This date already exists.");
            bindingResult.addError(error);
        }

        if (bindingResult.hasErrors()) {
            List<DentistDTO> dentists = dentistService.getDentists();
            model.addAttribute("dentists", dentists);
            return "visit/addVisit";
        }
        dentistVisitService.addVisit(visitDTO);
        return "redirect:/visit";
    }

    @GetMapping("/visits")
    public String viewVisits(Model model) {
        model.addAttribute("visits", dentistVisitService.getVisits());
        return "visit/visits";
    }

    @GetMapping("/details/{id}")
    public String showVisitDetails(Model model, @PathVariable("id") Long id) {
        DentistVisitDTO visitDTO = dentistVisitService.getVisit(id);
        if (visitDTO == null) {
            return "visit/visits";
        }
        List<DentistDTO> dentists = dentistService.getDentists();
        model.addAttribute("visit", visitDTO);
        model.addAttribute("dentists", dentists);
        return "visit/visitDetails";
    }

    @PostMapping("/details/{id}")
    public String changeVisitDetails(@PathVariable("id") Long id,
                                     @Valid DentistVisitDTO visitDTO, BindingResult bindingResult) {
        if (bindingResult.hasErrors()) {
            return "visit/visitDetails";
        }
        dentistVisitService.updateVisit(visitDTO);
        return "visit/visits";
    }

    @PostMapping("/delete/{id}")
    public String postDeleteVisit(@PathVariable("id") Long id) {
        DentistVisitDTO visit = dentistVisitService.getVisit(id);
        if (visit == null) {
            return "visit/visits";
        }
        dentistVisitService.deleteVisit(id);
        return "redirect:/dentist";
    }

    /*
    Dentist CRUD
     */

    @GetMapping("/dentist")
    public String showDentistRegistrationForm(Model model) {
        model.addAttribute("dentists", dentistService.getDentists());
        return "dentist/dentist";
    }

    @GetMapping("/dentist/add")
    public String addDentist(DentistDTO dentistDTO) {
        return "dentist/addDentist";
    }

    @PostMapping("/dentist/add")
    public String postDentistRegistrationForm(@Valid DentistDTO dentistDTO, BindingResult bindingResult) {
        if (bindingResult.hasErrors()) {
            return "dentist/addDentist";
        }
        dentistService.addDentist(dentistDTO);
        return "redirect:/dentist";
    }

    @PostMapping("/dentist/delete/{id}")
    public String postDeleteDentist(@PathVariable("id") Long id) {
        DentistDTO dentist = dentistService.getDentist(id);
        if (dentist == null) {
            return "error";
        }
        dentistService.deleteDentist(id);
        return "redirect:/dentist";
    }

    @GetMapping("/dentist/update/{id}")
    public String getUpdateDentist(Model model, @PathVariable("id") Long id) {
        DentistDTO dentistDTO = dentistService.getDentist(id);
        if (dentistDTO == null) {
            return "error";
        }
        model.addAttribute("dentist", dentistDTO);
        return "dentist/updateDentist";
    }

    @PostMapping("/dentist/update/{id}")
    public String postUpdateDentist(@PathVariable("id") Long id, @Valid DentistDTO dentistDTO, BindingResult bindingResult) {
        if (bindingResult.hasErrors()) {
            return "dentist/update/{id}";
        }
        dentistService.updateDentist(dentistDTO);
        return "redirect:/dentist";
    }
}