package com.cgi.dentistapp.mapper;

import com.cgi.dentistapp.dto.DentistDTO;
import com.cgi.dentistapp.dto.DentistVisitDTO;
import com.cgi.dentistapp.entity.DentistEntity;
import com.cgi.dentistapp.entity.DentistVisitEntity;
import lombok.NoArgsConstructor;
import org.modelmapper.ModelMapper;

import java.util.ArrayList;
import java.util.List;

@NoArgsConstructor
public class Mapper {

    ModelMapper modelMapper = new ModelMapper();

    public DentistDTO dentistEntityToDto(DentistEntity entity) {
        return modelMapper.map(entity, DentistDTO.class);
    }

    public DentistEntity dentistDtoToEntity(DentistDTO dentistDTO) {
        return modelMapper.map(dentistDTO, DentistEntity.class);
    }

    public DentistVisitDTO dentistVisitEntityToDto(DentistVisitEntity entity) {
        DentistVisitDTO visitDTO = modelMapper.map(entity, DentistVisitDTO.class);
        visitDTO.setDentistId(entity.getDentistEntity().getId());
        return visitDTO;
    }

    public DentistVisitEntity dentistVisitDtoToEntity(DentistVisitDTO dentistVisitDTO) {
        return modelMapper.map(dentistVisitDTO, DentistVisitEntity.class);
    }

    public List<DentistVisitDTO> dentistVisitEntityListToVisitDtoList(List<DentistVisitEntity> entities) {
        List<DentistVisitDTO> visitDTOS = new ArrayList<>();
        for (DentistVisitEntity entity : entities) {
            visitDTOS.add(dentistVisitEntityToDto(entity));
        }
        return visitDTOS;
    }
}
