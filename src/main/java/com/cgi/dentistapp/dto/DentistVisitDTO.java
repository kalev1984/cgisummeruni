package com.cgi.dentistapp.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.validator.constraints.Range;
import org.springframework.format.annotation.DateTimeFormat;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.Date;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class DentistVisitDTO {
    Long id;

    @NotNull
    @Size(min = 1, max = 50)
    private String firstName;

    @NotNull
    @Size(min = 1, max = 50)
    private String lastName;

    @NotNull
    @Range(min = 100000, max = 99999999)
    private Long phoneNumber;

    @NotNull
    @Size(min = 5, max = 50)
    private String email;

    @NotNull
    @DateTimeFormat(pattern = "dd.MM.yyyy hh:mm")
    Date date;

    @NotNull
    private Long dentistId;
}
