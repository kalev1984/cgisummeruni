package com.cgi.dentistapp.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.validator.constraints.Range;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class DentistDTO {
    Long id;

    @NotNull
    @Size(min = 5, max = 50)
    private String firstName;

    @NotNull
    @Size(min = 5, max = 50)
    private String lastName;

    @NotNull
    @Range(min = 6, max = 99999999)
    private Long phoneNumber;

    @NotNull
    @Size(min = 5, max = 50)
    private String email;
}
